<?php

use Illuminate\Database\Seeder;
use App\Item;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Item::class, 5)->create();
    }
}
