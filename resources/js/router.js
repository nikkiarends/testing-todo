import Vue from 'vue'
import Router from 'vue-router'
import ToDoList from './Components/ToDoList.vue'
import EditItem from './Components/EditItem.vue'
import NewItem from './Components/NewItem.vue'
import ItemShow from './Components/ItemShow.vue'
import ListView from './Components/ListView.vue'

Vue.use(Router)

const routes = [
    {
        path: '/newitem',
        name: 'newitem',
        component: NewItem,
    },
    {
        path: '/dashboard',
        name: 'items.dashboard',
        component: ToDoList,
    },
    {
        path: '/item/:id',
        name: 'item.show',
        component: ItemShow,

    },
    {
        path: '/item/:id/edit',
        name: 'item.edit',
        component: EditItem,
    },
    {
        path: '/list-view',
        name: 'list.view',
        component: ListView,
    },
]


export default new Router({
    routes,
});