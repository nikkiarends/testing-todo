import Vuex from 'vuex'
import Vue from 'vue'
import axios from './http'
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
    strict: true,
    state: {
        loading: false,
        allItems: [],
        message: '',
        errors: {}
    },

    getters: {},

    mutations: {
        LOADING(state, bool) {
            state.loading = bool
        },

        MESSAGE(state, message) {
            state.message = message
        },

        SET_ITEMS(state, allItems) {
            state.allItems = allItems
        },

        SET_ERRORS(state, errors) {
            state.errors = errors
        }


    },

    actions: {
        loadItems(store) {
            store.commit('LOADING', true)
            axios
                .get("/api/v1/items/")
                .then(response => response.data.items)
                .then(allItems => {
                    store.commit('SET_ITEMS', allItems)
                    store.commit('LOADING', false)
                })
        },

        deleteItem(store, item) {
            store.commit('LOADING', true)
            axios
                .delete("/api/v1/items/" + item.id)
                .then(response => {
                    console.log('store reached')
                    store.commit('SET_ITEMS', response.data.items)
                    router.push("/dashboard");
                    store.commit('MESSAGE', response.data.message)
                    store.commit('LOADING', false)
                }).catch(error => {
                    store.commit('SET_ERRORS', error.response.data.errors)
                })
        },

        addNewItem(store, {
            title,
            description,
            photo
        }) {
            store.commit('LOADING', true)
            axios
                .post("/api/v1/items/", {
                    title: title,
                    description: description,
                    photo: photo
                })
                .then(response => {
                    store.commit('SET_ITEMS', response.data.items)
                    router.push("/dashboard");
                    store.commit('MESSAGE', response.data.message)
                    store.commit('LOADING', false)
                }).catch(error => {
                    store.commit('SET_ERRORS', error.response.data.errors)
                })
        },

        editItem(store, item) {

            store.commit('LOADING', true)
            axios

                .put("/api/v1/items/" + item.id, {
                    title: item.title,
                    description: item.description,
                    photo: item.photo,
                    completed: item.completed
                })
                .then(response => {
                    store.commit('SET_ITEMS', response.data.items)
                    router.push("/dashboard");
                    store.commit('MESSAGE', response.data.message)
                    store.commit('LOADING', false)
                }).catch(error => {
                    store.commit('SET_ERRORS', error.response.data.errors)
                })

        },





    }

});
