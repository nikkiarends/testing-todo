import {
    mount,
    createLocalVue
} from "vue-test-utils";
import Vuex from "vuex";
import expect from "expect";
import EditItem from "../resources/js/components/EditItem.vue";
import store from "../../resources/js/store";
import moxios from "moxios";
import axios from "../../resources/js/http";
import VueRouter from "vue-router";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

const routes = [{
    path: "/item/:id/edit",
    component: EditItem
}];

const router = new VueRouter({
    routes
});

describe("EditItem", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(EditItem, {
            store,
            localVue,
            router
        });
        moxios.install(axios);
        if (wrapper.vm.$route.path != "/")
            router.push("/").catch(e => console.error(e));
    });

    afterEach(() => {
        // import and pass your custom axios instance to this method
        moxios.uninstall(axios);
    });

    it("shows a form to create a new item", () => {
        expect(wrapper.contains("#item-form")).toBe(true);
    });

    it("show the title of the item", () => {
        setItems()
        router
            .push("/item/1/edit")
            .then(_ => see(wrapper.vm.item.title))
            .catch(error => console.log(error));
    });

    it("shows a button to add the new item", () => {
        expect(wrapper.contains("button.save")).toBe(true);
    });

    it("shows input for entering the title, description and photo and a checkbox to check completed", () => {
        expect(wrapper.contains("input[name=title]")).toBe(true);
        expect(wrapper.contains("input[name=description]")).toBe(true);
        expect(wrapper.contains("input[name=photo]")).toBe(true);
        expect(wrapper.contains("input[type=checkbox][name=completed]")).toBe(
            true
        );
    });

    it("saves the changes at the click of the save button", done => {
        setItems()
        router
            .push("/item/1/edit")
            .then(function () {
                // console.log(wrapper.vm.item)
                type("input[name=title]", "changed title");
                type("input[name=description]", "changed description");
                type('input[name="photo"]', "some URL");
                wrapper.vm.item.completed = true

                moxios.wait(function () {
                    let request = moxios.requests.mostRecent();
                    request
                        .respondWith({
                            status: 200,
                            response: {
                                items: [{
                                    title: JSON.parse(request.config.data)
                                        .title,
                                    description: JSON.parse(
                                        request.config.data
                                    ).description,
                                    photo: JSON.parse(request.config.data)
                                        .photo,

                                    completed: JSON.parse(request.config.data)
                                        .completed
                                }]
                            }
                        })
                        .then(function () {
                            done();
                        });
                });
                click(".save");
            })
            .catch(error => console.log(error));
    });

    //Helper functions
    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
        expect(wrap.html()).toContain(text);
    };

    let click = selector => {
        wrapper.find(selector).trigger("click");
    };

    let type = (selector, text) => {
        let node = wrapper.find(selector);
        node.element.value = text;
        node.trigger("input");
    };

    let check = selector => {
        let node = wrapper.find(selector);
        node.element.checked = true;
        node.trigger("toggle");
    };

    function setItems() {
        store.state.allItems = [{
                id: 1,
                title: 'sample item',
                description: 'sample description',
                photo: 'https://hockeyzaak.nl/wp-content/uploads/2016/10/Perspectief-Rood2.png',
                completed: false,
            },
            {
                id: 2,
                title: 'second item',
                description: 'second description',
                photo: 'https://hockeyzaak.nl/wp-content/uploads/2016/10/Perspectief-Rood2.png',
                completed: false,
            }
        ]
    }
});
