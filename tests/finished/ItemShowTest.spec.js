import {
    mount,
    createLocalVue
} from 'vue-test-utils';
import expect from "expect";
import ItemShow from "../resources/js/components/ItemShow";
import store from "../../resources/js/store";
import VueRouter from "vue-router";
import Vuex from "vuex";
import moxios from "moxios";
import axios from "../../resources/js/http";
const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

const routes = [{
    path: '/item/:id',
    name: 'item.show',
    component: ItemShow,
}];

const router = new VueRouter({
    routes
});

describe("ItemShow", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(ItemShow, {
            store,
            localVue,
            router
        });
        moxios.install(axios);
        if (wrapper.vm.$route.path != "/")
            router.push("/").catch(e => console.error(e));
    });

    afterEach(() => {
        // import and pass your custom axios instance to this method
        moxios.uninstall(axios);
    });

    it("shows a button to delete the item", () => {
        setItems()
        router
            .push("/item/1")
            .then(_ => expect(wrapper.contains("button.delete")).toBe(true));
    });

    it("shows a button to edit the item", () => {
        setItems()
        router
            .push("/item/1")
            .then(_ => expect(wrapper.contains("#edit")).toBe(true))
    })

    it("shows the item's information", () => {
        setItems()
        router
            .push("/item/1")
            .then(function () {
                if (wrapper.vm.item.photo.length) {
                    let image = wrapper.find('img')
                    let imageAttributes = image.vnode.data.attrs.src
                    expect(imageAttributes).toBe(wrapper.vm.item.photo)
                }
                if (!wrapper.vm.item.photo) {
                    console.log('no photo registered')
                }
                see(wrapper.vm.item.description)
                see(wrapper.vm.item.title)
            })
            .catch(error => console.log(error));
    });

    it('shows item list', () => {
        console.log(store.state.allItems)
    })

    it('deletes item on button click', (done) => {
        setItems()
        router
            .push("/item/2")
            .then(function () {
                // console.log(store.state.allItems)
                moxios.wait(function () {
                    let request = moxios.requests.mostRecent();
                    request
                        .respondWith({
                            status: 200,
                            response: {
                                items: [{
                                    id: 1,
                                    title: 'sample item',
                                    description: 'sample description',
                                    photo: 'https://hockeyzaak.nl/wp-content/uploads/2016/10/Perspectief-Rood2.png',
                                    completed: false,
                                }, ]
                            }
                        }).then(function () {
                            done();
                            // console.log(store.state.allItems)
                        })
                })
                click("button.delete")
            })
            .catch(e => console.error(e));
    });

    it('shows updated list', () => {
        console.log(store.state.allItems)
    })


    //Helper functions
    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
        expect(wrap.html()).toContain(text);
    };

    let click = selector => {
        wrapper.find(selector).trigger("click");
    };

    let type = (selector, text) => {
        let node = wrapper.find(selector);
        node.element.value = text;
        node.trigger("input");
    };

    let check = selector => {
        let node = wrapper.find(selector);
        node.element.checked = true;
        node.trigger("toggle");
    };

    function setItems() {
        store.state.allItems = [{
                id: 1,
                title: 'sample item',
                description: 'sample description',
                photo: 'https://hockeyzaak.nl/wp-content/uploads/2016/10/Perspectief-Rood2.png',
                completed: false,
            },
            {
                id: 2,
                title: 'second item',
                description: 'second description',
                photo: 'https://hockeyzaak.nl/wp-content/uploads/2016/10/Perspectief-Rood2.png',
                completed: false,
            }
        ]
    }

});
