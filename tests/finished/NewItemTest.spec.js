import {
    mount,
    createLocalVue
}
from 'vue-test-utils';
import Vuex from 'vuex'
import expect from 'expect';
import NewItem from "../resources/js/components/NewItem.vue";
import store from "../../resources/js/store";
import moxios from 'moxios'
import axios from '../../resources/js/http'


const localVue = createLocalVue()
localVue.use(Vuex)


describe("NewItem", () => {
    let wrapper

    beforeEach(() => {
        wrapper = mount(NewItem, {
            store,
            localVue,
        });
        moxios.install(axios)
    });

    afterEach(() => {
        // import and pass your custom axios instance to this method
        moxios.uninstall(axios)
    });

    it('shows a form to create a new item', () => {
        expect(wrapper.contains('#item-form')).toBe(true);
    });

    it('shows a button to add the new item', () => {
        expect(wrapper.contains('button.save')).toBe(true);
    });

    it('shows input for entering the title, description and photo and a checkbox to check completed', () => {
        expect(wrapper.contains('input[name=title]')).toBe(true);
        expect(wrapper.contains('input[name=description]')).toBe(true);
        expect(wrapper.contains('input[name=photo]')).toBe(true);
        expect(wrapper.contains('input[type=checkbox][name=completed]')).toBe(true);
    });

    it('submits the item at the click of the submit button', (done) => {
        type('input[name=title]', 'Entered title');
        type('input[name=description]', 'Entered description');
        type('input[name=completed]', true);

        moxios.wait(function () {
            let request = moxios.requests.mostRecent()
            request.respondWith({
                    status: 200,
                    response: {
                        items: [{
                            title: JSON.parse(request.config.data).title,
                            description: JSON.parse(request.config.data).description,
                            photo: JSON.parse(request.config.data).photo,
                            complete: JSON.parse(request.config.data).complete
                        }]

                    },
                })
                .then(function () {
                    done()
                })
        })
        click('.save')
    });

    it('updates the item list in the store', () => {
        expect(store.state.allItems[0].title).toBe('Entered title')
        expect(store.state.allItems[0].description).toBe('Entered description')
    });



    //Helper functions
    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
        expect(wrap.html()).toContain(text);
    };

    let click = (selector) => {
        wrapper.find(selector).trigger('click');
    };

    let type = (selector, text) => {
        let node = wrapper.find(selector);
        node.element.value = text;
        node.trigger('input');
    }

});
