import {
    mount,
    createLocalVue
}
from 'vue-test-utils';
import Vuex from 'vuex'
import expect from 'expect';
import ToDoList from "../resources/js/components/ToDoList.vue";
import "regenerator-runtime/runtime";
import puppeteer from 'puppeteer'


const localVue = createLocalVue()

localVue.use(Vuex)


describe("ToDoList", () => {
    let wrapper, store;

    beforeEach(() => {
        store = new Vuex.Store({});
        wrapper = mount(ToDoList, {
            store,
            localVue,
            propsData: {
                items: [{
                    title: "test",
                    description: "test",
                }, ]
            }
        });



    });

    it('browser', async () => {
        console.log(puppeteer)
        const browser = await puppeteer.launch();

        // const page = await browser.newPage();
        // await page.goto('https://example.com');
        // await page.screenshot({
        //     path: 'screenshots/example.png'
        // });

        // await browser.close();
        // done();
    })



    it("renders a list of items", () => {
        see('itemlist')
    });

    it("shows a button to add a new item", () => {
        expect(wrapper.contains('router-link.new-item')).toBe(true);
    });

    it('shows a button to get a detailed view of an item', () => {
        expect(wrapper.contains('router-link.view-item')).toBe(true);
    });

    it("shows the item title and description", () => {
        wrapper.setProps({
            items: [{
                title: "test1",
                description: "description1",
            }, {
                title: "test2",
                description: "description2",
            }]
        });

        wrapper.vm.items.forEach(item => {
            see(item.title)
            see(item.description)
        });
        // see('test1')
        // see('test2')
        // see('description1')
        // see('description2')
    });

    it('shows a item photo if it is present', () => {
        wrapper.setProps({
            items: [{
                title: "test1",
                description: "description1",
                completed: false,
            }, {
                title: "test2",
                description: "description2",
                photo: "testphoto",
                completed: true,
            }]
        });
        wrapper.vm.items.forEach(item => {
            if (item.photo) {
                see(item.photo);
            } else {
                //maybe add a function that checks that if there is no photo, nothing appears, or maybe a sample photo?
            }
        });
    });

    it('shows a check mark when an item is completed', () => {
        wrapper.setProps({
            items: [{
                title: "test1",
                description: "description1",
                completed: false,
            }, {
                title: "test2",
                description: "description2",
                photo: "testphoto",
                completed: true,
            }]
        });
        wrapper.vm.items.forEach(item => {
            if (item.completed == true) {
                see('completed-text')

            }
            if (item.completed == false) {
                //shows nothing? I don't know how to write that
            }
        });
    })


    //Helper functions
    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
        expect(wrap.html()).toContain(text);
    };

    let click = selector => {
        wrapper.find(selector).trigger("click");
    };


});
