const puppeteer = require("puppeteer");

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    console.log("✓ going to item overview");
    await page.goto("http://127.0.0.1:8000/#/dashboard");
    //selecting item
    console.log("✓ clicking on show item button for the first item");
    await page.evaluate(() => {
        document.querySelector('a[href="#/item/1"]').click();
    });

    console.log("✓ clicking edit button");
    await page.evaluate(() => {
        document.querySelector('#edit').click();
    });
    //checking fields
    console.log("✓ shows input fields");

    console.log("   ✓ shows title field");
    try {
        await page.waitForSelector('input.title', {
            timeout: 2000
        })
        console.log('       ✓ The element appeared')
    } catch (error) {
        console.log("       X The element didn't appear.")
    }

    console.log("   ✓ shows description field");
    try {
        await page.waitForSelector('input.description', {
            timeout: 2000
        })
        console.log('       ✓ The element appeared')
    } catch (error) {
        console.log("       X The element didn't appear.")
    }

    console.log("   ✓ shows photo field");
    try {
        await page.waitForSelector('input.photo', {
            timeout: 2000
        })
        console.log('       ✓ The element appeared')
    } catch (error) {
        console.log("       X The element didn't appear.")
    }

    console.log("   ✓ shows completed checkbox");
    try {
        await page.waitForSelector('input.completed', {
            timeout: 2000
        })
        console.log('       ✓ The element appeared')
    } catch (error) {
        console.log("       X The element didn't appear.")
    }
    //entering new information
    console.log("✓ entering new title");
    await page.evaluate(() => document.querySelector('input.title').value = "")
    await page.focus('input.title')
    await page.keyboard.type('Changed title')

    console.log("✓ entering new description");
    await page.evaluate(() => document.querySelector('input.description').value = "")
    await page.focus('input.description')
    await page.keyboard.type('Changed description')

    console.log("✓ entering photo url");
    await page.evaluate(() => document.querySelector('input.photo').value = "")
    await page.focus('input.photo')
    await page.keyboard.type('https://hockeyzaak.nl/wp-content/uploads/2016/10/Perspectief-Rood2.png')

    console.log('✓ clicking checkbox');
    await page.evaluate(() => document.querySelector('input.completed').value = true);
    //saving item
    console.log("✓ clicking save button");
    await page.evaluate(() => {
        document.querySelector('button.save').click();
    });

    console.log("✓ going back to item overview");
    await page.goto("http://127.0.0.1:8000/#/dashboard");
    //result
    await page.screenshot({
        path: "screenshots/result.png",
        fullPage: true,
    });

    await browser.close();
})();
