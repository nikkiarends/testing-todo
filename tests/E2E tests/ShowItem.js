const puppeteer = require("puppeteer");

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();


    console.log("✓ going to item overview");
    await page.goto("http://127.0.0.1:8000/#/dashboard");

    console.log("✓ clicking on show item button for the first item");
    await page.evaluate(() => {
        document.querySelector('a[href="#/item/1"]').click();
    });

    console.log("✓ edit button should be present");
    try {
        await page.waitForSelector('#edit', {
            timeout: 2000
        })
        console.log('   ✓ The element appeared')
    } catch (error) {
        console.log("   X The element didn't appear.")
    }

    console.log("✓ delete button should be present");
    try {
        await page.waitForSelector('button.delete', {
            timeout: 2000
        })
        console.log('   ✓ The element appeared')
    } catch (error) {
        console.log("   X The element didn't appear.")
    }

    console.log("✓ clicking delete button");
    await page.evaluate(() => {
        document.querySelector('button.delete').click();
    });

    console.log("✓ going back to item overview");
    await page.goto("http://127.0.0.1:8000/#/dashboard");


    await page.screenshot({
        path: "screenshots/result.png",
        fullPage: true,
    });

    await browser.close();
})();
