const puppeteer = require("puppeteer");

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    console.log("✓ going to home page");
    await page.goto("http://127.0.0.1:8000/#/dashboard");

    console.log("✓ clicking on new item button");
    await page.evaluate(() => {
        document.querySelector('a.new-item').click();
    });

    console.log("✓ entering title");
    await page.focus('input.title')
    await page.keyboard.type('Sample title')

    console.log("✓ entering description");
    await page.focus('input.description')
    await page.keyboard.type('Sample description')

    // console.log("✓ entering empty desription");
    // await page.focus('input.description');
    // await page.keyboard.type('');

    // console.log("✓ entering empty title");
    // await page.focus('input.title');
    // await page.keyboard.type('');

    console.log("✓ checking completed checkbox");
    const checkbox = await page.$('input.completed');
    console.log("   checkbox value is: " + await (await checkbox.getProperty('checked')).jsonValue());
    console.log("       clicking checkbox")
    await checkbox.click();
    console.log("   checkbox value is now: " + await (await checkbox.getProperty('checked')).jsonValue());


    console.log("✓ clicking save button");
    await page.evaluate(() => {
        document.querySelector('button.save').click();
    });

    console.log("✓ alert should appear");
    try {
        await page.waitForSelector('div.alert', {
            timeout: 1000
        })
        console.log('   ✓ The element appeared')
    } catch (error) {
        console.log("   X The element didn't appear.")
    }

    console.log("       alert message text: ");

    //only logs the first error/alert
    const element = await page.$(".alert");
    const text = await (await element.getProperty('textContent')).jsonValue();
    console.log('       "' + text + '"')

    await page.screenshot({
        path: "screenshots/result.png",
        fullPage: true,
    });

    await browser.close();
})();
