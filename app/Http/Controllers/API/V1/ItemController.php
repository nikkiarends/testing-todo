<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItem;
use App\Item;

/**
 * @todo :: API controllers should return a response()->json($data)
 * or a resource (collection), see:
 * https://laravel.com/docs/5.8/eloquent-resources
 * or a status code
 */
class ItemController extends Controller
{

    public function index()
    {
        return response()->json([
            'items' => Item::all(),
        ]);
    }

    public function show(Item $item)
    {
        return response()->json([
            'item' => $item,
        ]);
    }

    public function update(StoreItem $request, Item $item)
    {

        $validated = $request->validated();
        $item->update($request->all());
        $item->completed = $request->completed;
        $item->save();

        return response()->json([
            'item' => $item,
            'message' => "Item $item->title aangepast",
            'items' => Item::all(),
        ]);

    }

//validation
    public function store(StoreItem $request)
    {
        $validated = $request->validated();

        $item = Item::create($validated);
        $item->completed = false;
        $item->save();

        return response()->json([
            'items' => Item::all(),
            'message' => "Item $item->title aangemaakt",
        ]);

    }

    public function destroy(Item $item)
    {
        $item->delete();
        return response()->json([
            'items' => Item::all(),
            'message' => "Item $item->title verwijderd",
        ]);
    }
}
